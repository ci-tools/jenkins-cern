#!/bin/bash

set -e

# Copy custom admin config file with a precreated token to the running Jenkins
podname=$(oc get pods -o jsonpath="{.items[*].metadata.name}" -l deploymentconfig=jenkins)

# The folder for the admin users is `admin_<random-number>` in the current Jenkins version.
# Retrieve the folder name and then use it to copy the file
admin_user_path=$(oc exec $podname -- bash -c 'ls -d /var/lib/jenkins/users/admin*')
oc cp test/admin-config.xml $podname:$admin_user_path/config.xml
