<!-- replaces content in xml file -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				version="1.0"
				xmlns:dyn="http://exslt.org/dynamic"
				xmlns:set="http://exslt.org/sets"
        extension-element-prefixes="dyn set">

  <xsl:param name="xpath"/><!-- XPath expression that indicates content to be replaced -->
  <xsl:param name="replacement"/><!-- new content -->
  <xsl:param name="disable-output-escaping" select="'no'"/><!-- set to yes if new content is already XML -->
  <xsl:variable name="replace-set" select="dyn:evaluate($xpath)" />

  <xsl:template match="@*|node()" name="identity">
	  <xsl:choose>
	    <xsl:when test="set:intersection(. , $replace-set)">
        <xsl:choose>
          <xsl:when test="$disable-output-escaping = 'yes'">
              <xsl:value-of select="$replacement" disable-output-escaping="yes"/>
          </xsl:when>
          <xsl:otherwise>
                <xsl:value-of select="$replacement" disable-output-escaping="no"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy>
          <xsl:apply-templates select="@*|node()" />
        </xsl:copy>
      </xsl:otherwise>
	  </xsl:choose>
  </xsl:template>
</xsl:stylesheet>