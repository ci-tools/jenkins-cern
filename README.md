Overview
========

This repo contains outdated and unmaintained OKD template for Jenkins. This template was meant to be used with OKD 3.X and Jenkins 2.1XX.X. 

The template do some assumptions that are not valid anymore on OKD 4.X, so if you want to deploy a new instance you have to:
* Create a new project and call it `test-openshift1` (otherwise old SSO is not gonna work)
* Add to your project a secret called `sshdeploykey` secret as shown [here](https://gitlab.cern.ch/paas-tools/openshift-app-manager/-/blob/master/templates/default_webservices.yaml#L64-73) (because it was deployed automatically in every OKD 3.X project)
* Go in `template/jenkins-cern.yaml` file and set ```apiVersion: template.openshift.io/v1``` instead of ```apiVersion: v1``` (because the apiVersion changed in OKD 4.X)
* Add the template to your project
* Create a new app from the template
* To make the dashboard work you need to edit the ```Route``` hostname to `test-openshift1.web.cern.ch` (because in OKD 3.X every route was  ${project-name}.web.cern.ch)

During 03/23 the docker image was upgraded as first step to migration to upstream Jenkins template (to know more look at https://gitlab.cern.ch/webservices/webframeworks-planning/-/issues/992)