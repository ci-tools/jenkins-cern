# CERN-customized Jenkins image for OpenShift

# Image built from https://github.com/openshift/jenkins
# Image sha is the one deployed by 4.12 tag as 02/03/2023.
# The 4.12 tag was introduced in paas by https://gitlab.cern.ch/paas-tools/okd4-install/-/merge_requests/1146

FROM quay.io/openshift/origin-jenkins@sha256:326efecf75371406f53f1bf20056384c74ab8ef063df5f06c90abd762810e756

# this is merged with labels from upstream image
LABEL k8s.io.display-name="Jenkins ${JENKINS_VERSION}"

# switch to root user
USER 0

# install CERN centos8 repo so we can install the CERN CA
COPY ./contrib/yumrepos /etc/yum.repos.d/

RUN yum install -y --disablerepo="*" --enablerepo=cern CERN-CA-certs centos-linux-repos epel-release && \
    curl -s -L http://cern.ch/linux/docs/krb5.conf -o /etc/krb5.conf && \
    chmod 664 /etc/passwd && \
    yum install -y libxslt xmlstarlet --disablerepo="*" --enablerepo=baseos --enablerepo=epel && \
    yum clean all

# deploy custom run script (run.cern)
COPY ./contrib/s2i /usr/libexec/s2i

# customize configuration files
COPY ./contrib/openshift /opt/openshift

# Install well-known SSH host keys for GitLab
COPY ./contrib/ssh /etc/ssh

# install extra plugins and ensure permissions are correct everywhere after adding our own files
# Plugins go to /opt/openshift/plugins in the image, then on first run they'll be copied to /var/lib/jenkins
RUN /usr/local/bin/install-plugins.sh /opt/openshift/extra_plugins_no_overwrite.txt && \
    /usr/local/bin/install-plugins.sh /opt/openshift/extra_plugins_overwriten_at_startup.txt && \
    chown -R 1001:0 /opt/openshift && \
    /usr/local/bin/fix-permissions /opt/openshift && \
    /usr/local/bin/fix-permissions /var/lib/jenkins

# switch back to unpriviledged user
USER 1001

# We will need this once we change the base image to latest.
# run our custom startup script instead of upstream one
# upstream ENTRYPOINT was ["/usr/bin/go-init", "-main", "/usr/libexec/s2i/run"]
# in https://github.com/openshift/jenkins/blob/master/2/Dockerfile.localdev
ENTRYPOINT ["/usr/bin/go-init", "-main", "/usr/libexec/s2i/run.cern"]
